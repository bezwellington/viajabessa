//
//  ViewController.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 27/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {

    //MARK:- Statements
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var iconImage: UIImageView!
    
    private var activityIndicatorView: UIView?
    private var travelPackageObjects: TravelPackageObjects!
    private var personDAO: DAO_Person!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        personDAO = DAO_Person.sharedInstance
        travelPackageObjects = TravelPackageObjects()
        loginButton.layer.cornerRadius = 8
        
        self.hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // when user click inside of LoginButton
    @IBAction func loginAction(_ sender: Any) {
        
        // email or password empty
        if(emailTextField.text!.isEmpty || passwordTextField.text!.isEmpty) {
            let alert = UIAlertController(title: "Atenção!", message: "Preencha todos os campos.", preferredStyle: .alert)
            self.present(alert, animated: true)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

            }))
        }
        else { //start else 1 - email or password NOT empty
            
            // start activity indicato
            activityIndicatorView = ActivityIndicator().startActivityIndicator(obj: self)
            
            // verify if login is correct
            personDAO.loginUser(withEmail: emailTextField.text!, password: passwordTextField.text!, { (result) -> Void in
                if result == success { // get data of user
                    self.personDAO.getLoggedUserInfo({ (data, result) in
                        if result == success {
                            self.personDAO.person = Person(json: data)
                            print("logado")
                            
                            // stop activity indicator
                            ActivityIndicator().stopActivityIndicator(obj: self, indicator: self.activityIndicatorView!)
                            
                            // user correct - go to MyPackagesViewController
                            self.performSegue(withIdentifier: "loginSegue", sender: nil)
                            
                            // get travel packages of user
                            self.personDAO.fetchTravels() { (travels,result) in
                                if result == success {
                                    for oneTravel in travels {
                                        var dict = oneTravel.value as! [String : Any]
                                        let cod = dict["cod"] as! String
                                        let sta = dict["state"] as! String
                                        print(oneTravel)
                                        // save travel packages using state and cod (state:"RJ" , cod:"3")
                                        self.createArrayTravelPackageBought(state: sta, cod: cod)
                                    }
                                }
                            }
                            
                        }
                        else { // start else 2
                            
                            
                        } // end else 2
                    })
                }
                else { // start end3
                    // email od pasword invalid
                    
                    ActivityIndicator().stopActivityIndicator(obj: self, indicator: self.activityIndicatorView!)
                    
                    
                    let alert = UIAlertController(title: "Atenção!", message: "Email ou senha inválido", preferredStyle: .alert)
                    self.present(alert, animated: true)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                    }))
                } // end else3
            })
            
        }// end else 1
        
        
    }
    
    @IBAction func skipAction(_ sender: Any) {
        personDAO.person = Person()
        self.performSegue(withIdentifier: "loginSegue", sender: nil)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        self.performSegue(withIdentifier: "registerSegue", sender: nil)
    }
    
    func createArrayTravelPackageBought(state: String, cod: String){
        let packs = travelPackageObjects.getTravelPackages(initials: state)
        
        for pack in packs {
            if pack.getCod() == cod{
                self.personDAO.person.travels.append(pack)
            }
        }
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

