//
//  MapViewController.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 30/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController {
    
    var travelPackageMap: TravelPackage = TravelPackage()
    @IBOutlet weak var navigatioonBar: UINavigationBar!
    @IBOutlet weak var travelPackageMapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setTitle()
        setMapView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func lefBarButtonItem(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    func setTitle() {
        self.navigatioonBar.topItem?.title = travelPackageMap.getTitle()
    }
    
    func setMapView() {
        
        let travelPackageLatitude = travelPackageMap.getPosition().getLatitude()
        let travelPackageLongitude = travelPackageMap.getPosition().getLongitude()
        
        var mapRegion = MKCoordinateRegion()
        mapRegion.center.latitude = travelPackageLatitude
        mapRegion.center.longitude = travelPackageLongitude
        mapRegion.span.latitudeDelta = 0.005
        mapRegion.span.longitudeDelta = 0.005
        self.travelPackageMapView.region = mapRegion
        
        setAnnotation(lati: travelPackageLatitude, long: travelPackageLongitude)
    }
    
    func setAnnotation(lati: CLLocationDegrees, long: CLLocationDegrees) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: lati, longitude: long)
        travelPackageMapView.addAnnotation(annotation)
    }
    

}
