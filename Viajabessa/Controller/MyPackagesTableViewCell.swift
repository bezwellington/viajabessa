//
//  MyPackagesTableViewCell.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 01/05/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit

class MyPackagesTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var travelImage: UIImageView!
    @IBOutlet weak var costLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
