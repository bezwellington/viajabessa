//
//  MyPackagesViewController.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 29/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit

class MyPackagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var personDAO: DAO_Person!
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        personDAO = DAO_Person.sharedInstance
        
        print("numero de elementos: \(personDAO.person.travels.count)")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.rowHeight = 60.0;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.personDAO.person.travels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifierMyTravels", for: indexPath) as! MyPackagesTableViewCell
        
        // Fetch Package
        let package = self.personDAO.person.travels[indexPath.row]
        
        // Configure Cell
        cell.titleLabel.text = package.getTitle()
        cell.costLabel.text = String(package.getCost())
        
        return cell
    }
    
    // MARK: - UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }

}
