//
//  RegisterViewController.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 01/05/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var paswordTextField: UITextField!
    
    private var personDAO: DAO_Person!
    private var activityIndicatorView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        personDAO = DAO_Person.sharedInstance

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        //
        if((self.emailTextField.text!.isEmpty) || (self.paswordTextField.text!.isEmpty) || (self.nameTextField.text!.isEmpty) ) {
            
            let alert = UIAlertController(title: "Atenção!", message: "Preencha todos os campos.", preferredStyle: .alert)
            self.present(alert, animated: true)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                
            }))
            
        }
        else { // name or email or password is empty
            
            if(paswordTextField.text!.count >= 6){
                
                if(emailTextField.text!.isValidEmail() == false){
                    
                    let alert = UIAlertController(title: "Email inválido", message: "ex: teste@teste.com", preferredStyle: .alert)
                    self.present(alert, animated: true)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                    }))
                    
                }
                else { // start else1 - email and password valid
                    
                    activityIndicatorView = ActivityIndicator().startActivityIndicator(obj: self)
                    
                    var info = [String:Any]()
                    info["name"] = self.nameTextField.text!
                    info["email"] = self.emailTextField.text!
                    
                    personDAO.createUser(email: self.emailTextField.text!, password: self.paswordTextField.text!) { (result) in
                        if result == success {
                            self.personDAO.savePersonalInfo(info, { (result) in
                                if result == success {
                                    print("usuário criado com sucesso")
                                    
                                    ActivityIndicator().stopActivityIndicator(obj: self, indicator: self.activityIndicatorView!)
                                    
                                    
                                    let alert = UIAlertController(title: "Parabéns!", message: "Cadastro efetuado com sucesso!", preferredStyle: .alert)
                                    self.present(alert, animated: true)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        self.dismiss(animated: true)
                                    }))
                                }
                            })
                            
                        }
                        else {
                            print(result)
                        }
                    }
                } //end else1
                
            }
            else {
                let alert = UIAlertController(title: "Senha inválida!", message: "A senha deve ter mais de 5 caracteres", preferredStyle: .alert)
                self.present(alert, animated: true)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    
                }))
            }
            
        
            
    
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
