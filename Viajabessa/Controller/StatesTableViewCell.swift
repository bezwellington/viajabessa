//
//  StatesTableViewCell.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 30/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit

class StatesTableViewCell: UITableViewCell {

    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var stateImage: UIImageView!
    @IBOutlet weak var stateAlpha: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
