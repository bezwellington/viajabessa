//
//  StatesViewController.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 30/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit

class StatesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var travelPackageObjects: TravelPackageObjects!
    var states: [String] = [String]()
    var statesImages: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        travelPackageObjects = TravelPackageObjects()
        states = travelPackageObjects.getStates()
        statesImages = travelPackageObjects.getStatesImages()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.rowHeight = 73.0;

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  segue.identifier == "statesSegue" {
            let destination = segue.destination as? TravelPackagesViewController
            let index = tableView.indexPathForSelectedRow?.row
            destination?.stateAbbreviation = statesImages[index!] //RJ
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.states.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "StateCell", for: indexPath) as! StatesTableViewCell
        
        // Fetch Package
        let stateName = self.states[indexPath.row]
        let stateImage = self.statesImages[indexPath.row]
        
        // Configure Cell
        cell.stateLabel.text = stateName
        cell.stateImage.image = UIImage(named: "\(stateImage)")
        cell.stateAlpha.alpha = 0.5
        
        return cell
    }
    
    // MARK: - UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let row = indexPath.row
        print(states[row])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
