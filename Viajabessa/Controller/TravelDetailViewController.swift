//
//  TravelDetailViewController.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 27/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class TravelDetailViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{

    var travelPackage: TravelPackage = TravelPackage()
    var states: [String]!
    var personDAO: DAO_Person!
    private var activityIndicatorView: UIView?
    
    @IBOutlet weak var navigatioonBar: UINavigationBar!
    @IBOutlet weak var travelPackageImage: UIImageView!
    @IBOutlet weak var travelPackageMapView: MKMapView!
    @IBOutlet weak var travelPackageTextView: UITextView!
    @IBOutlet weak var travelPackageCost: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.personDAO = DAO_Person.sharedInstance
        
        setTitle()
        setDescription()
        setImage()
        setCost()
        setMapView()
        
       travelPackageMapView.delegate = self
    }
    
    
    @IBAction func leftBarButtonItem(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func buyAction(_ sender: Any) {
        if personDAO.person.getEmail() == "" { // logged"
            
            let alert = UIAlertController(title: "Usuário Inválido", message: "Não é possível realizar uma compra sem cadastro", preferredStyle: .alert)
            self.present(alert, animated: true)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                self.dismiss(animated: true)
            }))
        
        }
        else { // not logged
            
            activityIndicatorView = ActivityIndicator().startActivityIndicator(obj: self)
            
            var info = [String:Any]()
            info["model"] = UIDevice.current.model
            info["systemName"] = UIDevice.current.systemName
            info["systemVersion"] = UIDevice.current.systemVersion
            
            var pack = [String:Any]()
            pack["state"] = travelPackage.getState()
            pack["cod"] = travelPackage.getCod()
            
            
            self.personDAO.saveMarketingInformation(info, { (result) in
                if result == success {
                    print("Informação de marketing salva com sucesso")
                    self.personDAO.saveTravelPackage(pack, { (result) in
                        if result == success {
                            print("Pacote de viagem salvo com sucesso")
                            self.personDAO.person.travels.append(self.travelPackage)
                            ActivityIndicator().stopActivityIndicator(obj: self, indicator: self.activityIndicatorView!)
                            
                            let alert = UIAlertController(title: self.travelPackage.getTitle(), message: "Compra efetuada com sucesso!", preferredStyle: .alert)
                            self.present(alert, animated: true)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                self.dismiss(animated: true)
                            }))
                            
                        }
                    })
                    
                }
            })
            
        }
        
    }
    
    func setTitle() {
        self.navigatioonBar.topItem?.title = travelPackage.getTitle()
    }
    
    func setDescription() {
        self.travelPackageTextView.text = travelPackage.getDescription()
    }
    
    func setImage() {
        self.travelPackageImage.image = UIImage(named: "\(travelPackage.getImage())")
    }
    
    func setCost() {
        self.travelPackageCost.text = getCostFormatted(cost: travelPackage.getCost())
    }
    
    func setMapView() {
        
        let travelPackageLatitude = travelPackage.getPosition().getLatitude()
        let travelPackageLongitude = travelPackage.getPosition().getLongitude()
        
        var mapRegion = MKCoordinateRegion()
        mapRegion.center.latitude = travelPackageLatitude
        mapRegion.center.longitude = travelPackageLongitude
        mapRegion.span.latitudeDelta = 0.005
        mapRegion.span.longitudeDelta = 0.005
        self.travelPackageMapView.region = mapRegion
        
        setAnnotation(lati: travelPackageLatitude, long: travelPackageLongitude)
    }
    
    func setAnnotation(lati: CLLocationDegrees, long: CLLocationDegrees) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: lati, longitude: long)
        travelPackageMapView.addAnnotation(annotation)
    }
    
    func getCostFormatted(cost: Float) -> String {
        return String("à vista " + "R$ " + String(cost) )
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func mapViewAction(_ sender: Any) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  segue.identifier == "goToMapViewController" {
            let destination = segue.destination as? MapViewController
            destination?.travelPackageMap = travelPackage
        }
    }
    

}
