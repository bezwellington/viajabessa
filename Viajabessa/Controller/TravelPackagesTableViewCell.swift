//
//  TravelTableViewCell.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 29/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit

class TravelPackagesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var packageTitle: UILabel!
    @IBOutlet weak var packageSubtitle: UILabel!
    @IBOutlet weak var packageCost: UILabel!
    @IBOutlet weak var packageImage: UIImageView!
    @IBOutlet weak var imageAlpha: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
