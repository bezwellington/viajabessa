//
//  MyTravelsViewController.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 27/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import UIKit
import Firebase

class TravelPackagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigatioonBar: UINavigationBar!
    
 
    var travelPackageObjects: TravelPackageObjects!
    var travelPackages: [TravelPackage] = [TravelPackage]()
    var stateAbbreviation = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        travelPackageObjects = TravelPackageObjects()
        
        
        // put objects inside of travelPackages
        travelPackages = travelPackageObjects.getTravelPackages(initials: stateAbbreviation)
    
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.rowHeight = 200.0;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigatioonBar.topItem?.title = travelPackageObjects.getNameOfState(initials: stateAbbreviation)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if  segue.identifier == "myTravelsSegue" {
            let destination = segue.destination as? TravelDetailViewController
            let index = tableView.indexPathForSelectedRow?.row
            destination?.travelPackage = travelPackages[index!]

            
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.travelPackages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath) as! TravelPackagesTableViewCell
        
        // Fetch Package
        let package = self.travelPackages[indexPath.row]
        
        // Configure Cell
        cell.packageTitle.text = package.getTitle()
        cell.packageSubtitle.text = package.getSubtitle()
        cell.packageCost.text = getCostFormatted(cost: package.getCost())
        cell.packageImage.image = UIImage(named: "\(package.getImage())")
        cell.imageAlpha.alpha = 0.5

        return cell
    }

    // MARK: - UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let row = indexPath.row
        print(travelPackages[row])
    }
    
    @IBAction func leftBarButtonItem(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    func getCostFormatted(cost: Float) -> String {
        return String("R$" + " " + String(cost) )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
