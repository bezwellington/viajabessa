//
//  DAO_Person.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 01/05/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import Foundation
import Firebase

class DAO_Person {
    
    //MARK:- Statements
    public var loggedUser: User?                        /* save uid of user */
    public var person = Person()                        /* save data of user */
    private var ref: DatabaseReference!                 /* reference for database */
    private static var theOnlyInstance: DAO_Person?
    
    init(){
        ref = Database.database().reference()
    }
    
    //MARK:- Singleton Definition
    static var sharedInstance: DAO_Person {
        get {
            if theOnlyInstance == nil {
                theOnlyInstance = DAO_Person()
            }
            return theOnlyInstance!
        }
    }
    
    //MARK:- Funtions

    // verifies that the user is registered in firebase
    public func loginUser(withEmail email: String, password: String, _ completion: @escaping ((_ result: String) -> Void)) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error == nil {
                self.loggedUser = Auth.auth().currentUser
                completion(success)
            }
            else {
                completion((error?.localizedDescription)!)
            }
        }
    }
    
    // function that get all data of user (name, email, ...)
    public func getLoggedUserInfo( _ completion: @escaping ((_ data: [String : Any], _ result: String) -> Void)) {
        let uid = (loggedUser?.uid)!
        read("", withKey: uid, at: "users") { (data, result) in
            if result == success {
                if !data.isEmpty {
                    completion(data, success)
                }
            }
            else {
                completion([:], result)
            }
        }
    }
    
    // function that returns a block of data from firebase
    func read(_ identifier: Any, withKey key: String, at path: String, _ completion: @escaping ((_ data: [String : Any], _ result: String) -> Void)) {
        let ref = Database.database().reference().child(path).child("\(key)/\(identifier)")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.value is NSNull {
                completion([:], "Não existe nenhum dado no diretório requisitado.")
            }
            else {
                completion(snapshot.value as! [String : Any], success)
            }
        }) { (error) in
            completion([:], error.localizedDescription)
        }
    }
    
    // function that create a user on firebase
    public func createUser(email: String, password: String, _ completion: @escaping ((_ result: String) -> Void)) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error == nil {
                self.loginUser(withEmail: email, password: password, completion)
            }
            else {
                completion((error?.localizedDescription)!)
            }
        }
    }
    
    // function that save info(email, name, ...) of user on firebase
    public func savePersonalInfo(_ info: [String : Any], _ completion: @escaping ((_ result: String) -> Void)) {
        create(object: info, at: "users", uid: (loggedUser?.uid)!, completion)
    }
    
    // function that save marketion information(model, systemName, systemVersion of cell phone)
    public func saveMarketingInformation(_ info: [String : Any], _ completion: @escaping ((_ result: String) -> Void)) {
        createWithAutoId(object: info, at: "marketing", completion)
    }
    
    // function that save, on firebase, the travel package purchased by the user
    public func saveTravelPackage(_ pack: [String : Any], _ completion: @escaping ((_ result: String) -> Void)) {
        let path = loggedUser!.uid
        createWithAutoId(object: pack, at: "travels/\(path)/", completion)
    }
    
    // function that returns all trips purchased by the user
    public func fetchTravels(_ completion: @escaping ((_ data: [String : Any], _ result: String) -> Void)) {
        let uid = (loggedUser?.uid)!
        read("", withKey: uid, at: "travels", completion)
    }
    
    // function that saves a data in the firebase using a random key
    func createWithAutoId(object: [String:Any], at path: String, _ completion: @escaping ((_ result: String) -> Void)) {
        var result = success
        
        Database.database().reference().child(path).childByAutoId().setValue(object, withCompletionBlock: { (error, ref) in
            if (error != nil) {
                result = (error?.localizedDescription)!
            }
            
            completion(result)
        })
    }
    
    // function that saves a data in the firebase NOT using a random key
    func create(object: [String:Any], at path: String, uid: String, _ completion: @escaping ((_ result: String) -> Void)) {
        var result = success
        
        Database.database().reference().child(path).child(uid).setValue(object, withCompletionBlock: { (error, ref) in
            if (error != nil) {
                result = (error?.localizedDescription)!
            }
            
            completion(result)
        })
    }
    
    
    
    
}
