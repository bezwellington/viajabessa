//
//  TravelPackageObjects.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 30/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import Foundation

class TravelPackageObjects {
    
    private let pack1 = TravelPackage(cod: "1", title: "Praiana Resort", subtitle: "Inclui buffet e café da manhã", state:"AC", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 245.00, image: "img1", position: Position(latitude: -22.9807115, longitude: -43.1895503))
    
    private let pack2 = TravelPackage(cod: "2", title: "Pool Resort", subtitle: "Inclui buffet e café da manhã", state:"AC", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 386.00, image: "img2", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack3 = TravelPackage(cod: "3", title: "Luxo Resort", subtitle: "Inclui buffet e café da manhã", state:"AC", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 568.00, image: "img3", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack4 = TravelPackage(cod: "4", title: "Porto Seguro Praia Resort", subtitle: "Inclui buffet e café da manhã", state:"AL", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 868.00, image: "img4", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack5 = TravelPackage(cod: "5", title: "Maçambaba Resort", subtitle: "Inclui buffet e café da manhã", state:"AL", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 333.00, image: "img5", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack6 = TravelPackage(cod: "6", title: "Acqua Marine Park Hotel", subtitle: "Inclui buffet e café da manhã", state:"AL", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 123.00, image: "img6", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack7 = TravelPackage(cod: "7", title: "Hot Beach Olimpia", subtitle: "Inclui buffet e café da manhã", state:"AP", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 654.00, image: "img7", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack8 = TravelPackage(cod: "8", title: "Aldeia das águas", subtitle: "Inclui buffet e café da manhã", state:"AP", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 345.00, image: "img8", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack9 = TravelPackage(cod: "9", title: "Bourbon Atibaia", subtitle: "Inclui buffet e café da manhã", state:"AP", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 519.00, image: "img9", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack10 = TravelPackage(cod: "10", title: "Thermas Park Resort", subtitle: "Inclui buffet e café da manhã", state:"AM", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 400.00, image: "img10", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    private let pack11 = TravelPackage(cod: "11", title: "Rio Water Planet", subtitle: "Inclui buffet e café da manhã", state:"AM", description: "Resort 5 estrelas situado em frente à Praia de Curuípe, o Porto Seguro Praia Resort - All Inclusive, dispõe de ampla área de lazer com piscina, academia, sauna, parque de aventuras e muito mais. O hotel possui uma academia e estrutura para eventos.As acomodações são equipadas com ar-condicionado, TV a cabo, Wi-Fi e banheiro privativo.", cost: 299.00, image: "img11", position: Position(latitude: +32.93644619, longitude: -96.72057518))
    
    func getTravelPackages(initials: String) -> [TravelPackage] {
        
        var dict = [String: [TravelPackage]]()
        dict = ["AC":[pack1, pack2, pack3] , "AL":[pack4, pack5, pack6], "AP":[pack4, pack5, pack6], "AM":[pack10, pack11], "BA":[pack4, pack5, pack6], "CE":[pack1, pack2, pack3], "DF":[pack4, pack5, pack6], "ES":[pack1, pack2, pack3], "GO":[pack4, pack5, pack6], "MA":[pack4, pack5, pack6], "MT":[pack1, pack2, pack3], "MS":[pack4, pack5, pack6], "MG":[pack4, pack5, pack6], "PA":[pack4, pack5, pack6], "PB":[pack1, pack2, pack3], "PR":[pack4, pack5, pack6], "PE":[], "PI":[pack1, pack2, pack3], "RJ":[pack4, pack5, pack6], "RN":[pack4, pack5, pack6], "RS":[pack1, pack2, pack3], "RO":[pack4, pack5, pack6], "RR":[pack1, pack2, pack3], "SC":[pack4, pack5, pack6], "SP":[pack1, pack2, pack3], "SE":[pack4, pack5, pack6], "TO":[pack4, pack5, pack6]]
        
        return dict[initials]!
    }
    
    func getNameOfState(initials: String) -> String {
        
        var dict = [String: String]()
        dict = ["AC":"Acre" , "AL":"Alagoas", "AP":"Amapá", "AM":"Amazonas", "BA":"Bahia", "CE":"Ceará", "DF":"Distrito Federal", "ES":"Espírito Santo", "GO":"Goiás", "MA":"Maranhão", "MT":"Mato Grosso", "MS":"mATO grosso do Sul", "MG":"Minas gerais", "PA":"Pará", "PB":"Paraíba", "PR":"Paraná", "PE":"Pernambuco", "PI":"Piauí", "RJ":"Rio de Janeiro", "RN":"Rio Grande do Norte", "RS":"Rio Grande do Sul", "RO":"Rondônia", "RR":"Roraima", "SC":"Santa Catarina", "SP":"São Paulo", "SE":"Sergipe", "TO":"Tocantins"]
        
        return dict[initials]!
    }
    
    func getStates() -> [String] {
        var array:[String] = []
        array = [
            "Acre (AC)",
            "Alagoas (AL)",
            "Amapá (AP)",
            "Amazonas (AM)",
            "Bahia (BA)",
            "Ceará (CE)",
            "Distrito Federal (DF)",
            "Espírito Santo (ES)",
            "Goiás (GO)",
            "Maranhão (MA)",
            "Mato Grosso (MT)",
            "Mato Grosso do Sul (MS)",
            "Minas Gerais (MG)",
            "Pará (PA)",
            "Paraíba (PB)",
            "Paraná (PR)",
            "Pernambuco (PE)",
            "Piauí (PI)",
            "Rio de Janeiro (RJ)",
            "Rio Grande do Norte (RN)",
            "Rio Grande do Sul (RS)",
            "Rondônia (RO)",
            "Roraima (RR)",
            "Santa Catarina (SC)",
            "São Paulo (SP)",
            "Sergipe (SE)",
            "Tocantins (TO)" ]
        return array
    }
    
    func getStatesImages() -> [String] {
        var array:[String] = []
        array = ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ", "RJ"]
        return array
    }
}
