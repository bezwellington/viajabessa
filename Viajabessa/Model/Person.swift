//
//  Pessoa.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 27/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import Foundation

class Person {
    
    //MARK:- Statements
    private var name: String!                   /* name of user */
    private var email: String!                  /* email of user */
    private var phone: String!                  /* phone of user */
    private var dateBirth: String!              /* dateBirth of user */
    private var position: Position!             /* position of user */
    public var travels = [TravelPackage]()      /* travel packages purchased by the user */
    
    init(name: String, email: String) {
        self.name = name
        self.email = email
    }
    
    init(json: [String:Any]) {
        self.name = json["name"] as! String
        self.email = json["email"] as! String
    }
    
    init() {
        self.name = ""
        self.email = ""
        self.phone = ""
        self.dateBirth = ""
    }
    
    //MARK:- Functions
    
    // function that return name of person
    func getName() -> String {
        return self.name
    }
    
    // function that return email of person
    func getEmail() -> String {
        return self.email
    }
    
    // function that return phone of person
    func getPhone() -> String {
        return self.phone
    }
    
    // function that return dateBirth of person
    func getDateBirth() -> String {
        return self.dateBirth
    }
    
    // function that return Position of person
    func getPosition() -> Position {
        return self.position
    }
    
    // function that return package travels of person
    func getTravels() -> [TravelPackage] {
        return self.travels
    }
    
    
}
