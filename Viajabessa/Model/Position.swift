//
//  Pais.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 27/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import Foundation
import CoreLocation

class Position {
    
    //MARK:- Statements
    
    private var longitude: CLLocationDegrees!       /* longitude of positon */
    private var latitude: CLLocationDegrees!        /* latitude of positon */
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    //MARK:- Functions
    
    // fucntion that return latitude
    func getLatitude() -> CLLocationDegrees{
        return self.latitude
    }
    
    // fucntion that return longitude
    func getLongitude() -> CLLocationDegrees{
        return self.longitude
    }
}
