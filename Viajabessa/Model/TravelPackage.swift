//
//  PacoteViagem.swift
//  Viajabessa
//
//  Created by Wellington Bezerra on 27/04/18.
//  Copyright © 2018 Wellington Bezerra. All rights reserved.
//

import Foundation

class TravelPackage {
    
    //MARK:- Statements
    
    private var cod: String!            /* cod of TravelPackage*/
    private var title: String!          /* title of TravelPackage*/
    private var subtitle: String!       /* subtitle of TravelPackage*/
    private var state: String!          /* state of TravelPackage*/
    private var description: String!    /* description of TravelPackage*/
    private var cost: Float!            /* cost of TravelPackage*/
    private var image: String!          /* image of TravelPackage*/
    private var position: Position!     /* positon of TravelPackage*/
    
    init(cod: String, title: String, subtitle: String, state: String, description: String, cost: Float, image: String, position: Position) {
        self.cod = cod
        self.title = title
        self.subtitle = subtitle
        self.state = state
        self.description = description
        self.cost = cost
        self.image = image
        self.position = position
    }
    
    init() {
        self.title = nil
        self.subtitle = nil
        self.cost = nil
        self.image = nil
        self.position = nil
    }
    
    //MARK:- Functions
    
    //function that return cod of travelPackage
    func getCod() -> String{
        return self.cod
    }
    
    //function that return title of travelPackage
    func getTitle() -> String{
        return self.title
    }
    
    //function that return susbtitle of travelPackage
    func getSubtitle() -> String{
        return self.subtitle
    }
    
    //function that return state of travelPackage
    func getState() -> String{
        return self.state
    }
    
    //function that return description of travelPackage
    func getDescription() -> String{
        return self.description
    }
    
    //function that return cost of travelPackage
    func getCost() -> Float{
        return self.cost
    }
    
    //function that return imageName of travelPackage
    func getImage() -> String{
        return self.image
    }
    
    //function that return position of travelPackage
    func getPosition() -> Position{
        return self.position
    }
    
    
    
}
